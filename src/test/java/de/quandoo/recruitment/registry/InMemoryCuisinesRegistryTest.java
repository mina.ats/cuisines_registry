package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Before
    public void shouldWork1() {
        Customer customer1=new Customer("1");
        Customer customer2=new Customer("2");
        Customer customer3=new Customer("3");
        Customer customer4=new Customer("4");
        Customer customer5=new Customer("5");
        Customer customer6=new Customer("6");
        Customer customer7=new Customer("7");
        Customer customer8=new Customer("8");
        Customer customer9=new Customer("9");
        Customer customer10=new Customer("10");

        Cuisine cuisineFrench=new Cuisine("french");
        Cuisine cuisineGerman=new Cuisine("german");
        Cuisine cuisineItalian=new Cuisine("italian");

        List<Cuisine> cuisineList=new ArrayList<>();
        cuisineList.add(cuisineFrench);
        cuisineList.add(cuisineGerman);
        cuisineList.add(cuisineItalian);
        cuisinesRegistry=new InMemoryCuisinesRegistry(cuisineList);

        cuisinesRegistry.register(customer1, cuisineFrench);
        cuisinesRegistry.register(customer1, cuisineItalian);
        cuisinesRegistry.register(customer1, cuisineGerman);
        cuisinesRegistry.register(customer2, cuisineFrench);
        cuisinesRegistry.register(customer3, cuisineItalian);
        cuisinesRegistry.register(customer3, cuisineGerman);
        cuisinesRegistry.register(customer4, cuisineItalian);
        cuisinesRegistry.register(customer5, cuisineItalian);
        cuisinesRegistry.register(customer6, cuisineItalian);
        cuisinesRegistry.register(customer7, cuisineItalian);
        cuisinesRegistry.register(customer7, cuisineGerman);
        cuisinesRegistry.register(customer7, cuisineGerman);
        cuisinesRegistry.register(customer8, cuisineFrench);
        cuisinesRegistry.register(customer8, cuisineItalian);
        cuisinesRegistry.register(customer9 , cuisineItalian);
        cuisinesRegistry.register(customer10 , cuisineItalian);
        cuisinesRegistry.register(customer10 , cuisineGerman);
    }

    @Test
    public void shouldWork2() {
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        customerList.stream().forEach(System.out::println);
        assertEquals("count of customers is not correct",customerList.size(), 3);
    }
    @Test
    public void shouldWork3() {
        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(new Customer("1"));
        cuisineList.stream().forEach(System.out::println);
        assertEquals("count of customer cuisines is not correct", cuisineList.size(), 3);
    }

    @Test
    public void thisDoesntWorkYet() {
        List<Cuisine> cuisineList = cuisinesRegistry.topCuisines(1);
        cuisineList.stream().forEach(System.out::println);
        assertEquals("top cuisine must be","italian",cuisineList.get(0).getName());
    }
}