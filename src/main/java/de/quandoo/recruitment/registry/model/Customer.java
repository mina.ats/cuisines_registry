package de.quandoo.recruitment.registry.model;

public class Customer {
    private String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        if(obj instanceof Customer){
            Customer other= (Customer) obj;
            if(uuid.equals(other.getUuid())){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "customer id "+uuid.toString();
    }
}
