package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        if(obj instanceof Cuisine) {
            Cuisine other = (Cuisine) obj;
            return name.equals(other.getName());
        }else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "cuisine "+name;
    }
}
