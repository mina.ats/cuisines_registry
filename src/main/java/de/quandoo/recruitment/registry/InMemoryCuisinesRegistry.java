package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine,List<Customer>> cuisineCustomersMap;

    private InMemoryCuisinesRegistry(){}

    public InMemoryCuisinesRegistry(List<Cuisine> registeredCuisine){
        cuisineCustomersMap=new HashMap<>();
        registeredCuisine.forEach(cuisine -> cuisineCustomersMap.put(cuisine,new ArrayList<>()));
    }

    @Override
    public void register(Customer customer, Cuisine cuisine) {
        if(cuisineCustomersMap.get(cuisine)!=null){
            List<Customer> customerList = Collections.synchronizedList(cuisineCustomersMap.get(cuisine));
            customerList.add(customer);
        }else{
            throw new RuntimeException("Unknown cuisine, please reach johny@bookthattable.de to update the code "+cuisine.getName());
        }
    }

    @Override
    public List<Customer> cuisineCustomers(Cuisine cuisine) {

        List<Customer> customerList=new ArrayList<>();

        if(cuisineCustomersMap.get(cuisine)!=null) {
            customerList = cuisineCustomersMap.get(cuisine);
        }
        return customerList;
    }

    @Override
    public List<Cuisine> customerCuisines(Customer targetCustomer) {
        List<Cuisine> cuisineList = cuisineCustomersMap.entrySet().parallelStream().
                filter(entry ->
                        entry.getValue().stream().
                                filter(customer -> customer.equals(targetCustomer)).count() > 0)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
        return cuisineList;
    }

    @Override
    public List<Cuisine> topCuisines(int n) {
        // the n is confusing, it is not clear enough
        // I returned first n cuisines
        List<Cuisine> cuisineList = cuisineCustomersMap.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().size()))
                .entrySet().stream().sorted(Map.Entry.<Cuisine,Integer>comparingByValue().reversed())
                .map(entry -> entry.getKey()).collect(Collectors.toList()).subList(0,n);
        return cuisineList;
    }
}
