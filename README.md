# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution
First of all, I cleaned class InMemoryCuisinesRegistry by putting a map for keeping cuisines and their customers, and in register method I just get cuisine customer list from map and add it by considering concurrency problem.
I removed final keyword from cuisine and customer class cause there is no setter method for their fields and I removed it from InMemoryCuisinesRegistry methods too.
for finding topCuisines, I found every cuisine customers size then I sorted map by its value(customer list size) then return first n cuisines.

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).